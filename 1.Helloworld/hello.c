/* hello.c */

#include <linux/kernel.h>
#include <linux/module.h>
MODULE_LICENSE("GPL");

int init_module(void)
{
      printk("Hello world!\n"); /* printk = kernel printf, to the console */

        return 0;
}

void cleanup_module(void)
{
      printk("Goodbye world!\n");

        return;
}

/* end of simple.c */
