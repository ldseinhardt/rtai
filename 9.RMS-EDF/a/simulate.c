
/*
    simulate.c
*/

/* ------------ headers ------------------------------------------- */

#include <linux/kernel.h>    /* decls needed for kernel modules */
#include <linux/module.h>    /* decls needed for kernel modules */
#include <linux/version.h>    /* LINUX_VERSION_CODE, KERNEL_VERSION() */
#include <linux/errno.h>    /* EINVAL, ENOMEM */




/*
  Specific header files for RTAI, our flavor of RT Linux
 */
#include "rtai.h"
#include "rtai_sched.h"
#include <rtai_sem.h>

MODULE_LICENSE("GPL");


/* ------------ globals ------------------------------------------- */


// realtime task structures
#define NTASKS 3
#define STACK_SIZE 10000
static RT_TASK tasks[NTASKS];
static int task_arg[NTASKS];

// periodic scheduling
#define BASE_PERIOD_NS 1000000
static RTIME base_period;  // in internal units
static RTIME base_period_us; // in microseconds
static RTIME base_period_ns; // in nanoseconds

// task periodic times
static int task_computation_time[NTASKS] = { 30 , 20 ,  20 };
static int task_period[NTASKS]           = { 60 , 90 , 110 } ; 
//RMS : shortes period highest priority
static int task_priority[NTASKS]         = { 10 , 20 ,  30 } ;
static int ggd=1980;  // 6*9*11/3


 


RTIME resumetime[NTASKS];
RTIME deadlinetime[NTASKS];
int nodeadlinemiss=1;


// semaphore used for synchronised start
static SEM sync;
// place to store global start time
static RTIME tasks_starttime=0;


// count switched between tasks
static int switchesCount[NTASKS];

//  busy loop parameters
#define CALIBRATION_PERCENTAGE 100 

// set as high as possible!! the higher the value the more accurate we can
// calculate the calibration sleep parameter (the runtime of the overhead code is relatively less!)
// for too high values, the kernel thinks something hangs, 
// and aborts with a SOFT-LOCKUP warning -> than you are to high
#define CALIBRATION_LOOP_SIZE  1e7
static RT_TASK init_task_str;
//static RTIME count_need_for_one_us_busysleep;
static RTIME count_need_for_one_ms_busysleep;

// bookkeeping parameters
static int task_period_counter[NTASKS]         = { 0 ,  0 ,  0 } ;

// executes a loop from 0 till count
//  returns : time in ns to execute the loop
inline RTIME loop(RTIME count) {
    unsigned long int i;
    RTIME starttime;
    RTIME sleeptime_ns;

    starttime = rt_get_time_ns();
    for (i = 0; i < count ; i++) {}
    sleeptime_ns=rt_get_time_ns()-starttime;

    return sleeptime_ns;
}    

/* function to callibrate busysleep function */
void calibrate_busysleep(void)
{
    RTIME sleeptime_ns;
    RTIME x;
    

    volatile RTIME loop_size=CALIBRATION_LOOP_SIZE;
    
    // loop with global CALIBRATION_LOOP_SIZE
    sleeptime_ns=loop(loop_size);   
    rt_printk("sleeptime_ns=%lld\n",sleeptime_ns);
    
    // EXPLANATION CALCULATION :
    //    sleeptime_in_us = sleeptime_ns/1000 
    //    count_need_for_one_us_busysleep=calibration_loop_size/sleeptime_in_us;
    //     -> add calibration factor to sleeptime_in_us :
    //           sleeptime_in_us -> sleeptime_in_us *  calibrationpercentage/100
    //     -> combine everything 
    //        counterbusysleepns= calibration_loop_size*10*calibrationpercentage/sleeptime_ns
    x=CALIBRATION_LOOP_SIZE*10*CALIBRATION_PERCENTAGE*1000;
    do_div(x,sleeptime_ns);

   
    // set global calibration value
    //count_need_for_one_us_busysleep =x;
    count_need_for_one_ms_busysleep=x;
}


/*
    RTIME busysleep(RTIME sleeptime_us ) 

      usage :    
          sleeptime_ns busysleep(sleeptime_us) 
    
      description :    
          keep the processor busy in a loop for a sleeptime_us amount 
          of microseconds

      returns :       
          the real time passed during this loop  (if other tasks get
          scheduled during this function this time can become much larger than
          the time this function keeps the cpu busy)
*/          
RTIME busysleep(RTIME sleeptime_us ) {
    RTIME temp;
    
    RTIME sleeptime_ns;
    RTIME sleep_count;


    sleep_count= count_need_for_one_ms_busysleep*sleeptime_us;
    do_div(sleep_count,1000);

    //rt_printk("sleeptime_us=%lld\n", sleeptime_us); 
    //rt_printk("count_need_for_one_us_busysleep=%lld\n", count_need_for_one_us_busysleep); 
    //rt_printk("sleep_count=%lld\n", count_need_for_one_us_busysleep); 
    sleeptime_ns=loop(sleep_count);   
    return sleeptime_ns;
}



// calibrate busy sleep
static void init(long arg)
{
    rt_printk("------------------------------------------ init task  started\n",arg);

    // first calibrate busysleep for the speed of the current machine
    // no other tasks are allowed to run to garantee optimal calibration
    calibrate_busysleep();
    rt_printk("count_need_for_one_ms_busysleep=%lld\n", count_need_for_one_ms_busysleep); 
    //rt_printk("count_need_for_one_us_busysleep=%lld\n", count_need_for_one_us_busysleep); 
    
    rt_printk("------------------------------------------ init task  ended\n",arg);
    return;
}

// calculated time in  base_period's  
// time_in_base_periods=(rt_get_time()-starttime )/base_period  
// -> integer part :
RTIME time_int(RTIME temp)
{    
    do_div(temp,base_period);
    return temp;
}
// -> first two digits  :
RTIME time_digits(RTIME temp)
{    
    RTIME rest;
    rest=do_div(temp,base_period);
    temp=rest*100;
    do_div(temp,base_period);
    return temp;
}

// get time  since start
RTIME get_time() 
{
    RTIME temp;
    temp=rt_get_time()-tasks_starttime;
    return temp;
}    

// print info about task status
void print_info(int currenttask,char *msg) {
    RTIME now;
    now=get_time();
    rt_printk("T%d %s - time %3lld.%02lld  -  computation %3d  - period %3d - interval %d-%d \n",currenttask, msg,   time_int(now),time_digits(now),  task_computation_time[currenttask], task_period[currenttask], task_period_counter[currenttask]*task_period[currenttask], (task_period_counter[currenttask]+1)*task_period[currenttask]);
}

// task which calibrates busysleep and tests it
void dummy_task(long t)
{
    RTIME cur_task_sleeptime_ns;
    RTIME cur_task_sleeptime_us;
    RTIME base_periods_passed;
    RTIME cur_task_period;
    
    
    cur_task_period=task_period[t]*base_period;
    cur_task_sleeptime_us=base_period_us*task_computation_time[t];
    task_period_counter[t]=0;
    base_periods_passed=0;    
   
    
    if (!tasks_starttime) tasks_starttime=rt_get_time();
    
    while( time_int(get_time())  < ggd && nodeadlinemiss ) {
        //initialize data for scheduling
        resumetime[t]=tasks_starttime + (task_period_counter[t]*cur_task_period);
        deadlinetime[t]=resumetime[t]+cur_task_period;
        
        // start new run
        print_info(t,"start  ");



        // do busysleep 
        //rt_printk("   %d SLEEPTIME in us : %lld.\n",t,cur_task_sleeptime_us);        
        cur_task_sleeptime_ns=busysleep(cur_task_sleeptime_us);
        //rt_printk("   %d RESULT SLEEPTIME in ns : %lld.\n",t,cur_task_sleeptime_ns);

        // check  if deadline not passed
        // because of inaccuracy  of busysleep we allow deviations  smaller than base_period
        if (  rt_get_time()  >=  deadlinetime[t] + base_period ) { 
             rt_printk("\n\n\n");
             rt_printk("  TASK %d missed deadline %d", t, (task_period_counter[t]+1)*task_period[t] ); 
             rt_printk("\n\n\n");
             nodeadlinemiss=0;
        }
                
        // end current run
        print_info(t,"stop   ");
        rt_task_wait_period();        

        task_period_counter[t]++;     
    }
    print_info(t,"ended  ");
    if (nodeadlinemiss) rt_printk("\n\n  ggd reached - SCHEDULABLE\n\n ", t);
    
    return;
}




// start the realtime tasks with a specific scheduling
int init_module(void)
{
    int i;
    RTIME temp,starttime;
    printk("Start of init_module\n");
   
    printk("INSMOD on CPU %d.\n", hard_cpu_id());
    rt_sem_init(&sync, 0);
    
    
    
  

    
   
    
    // configure period mode
    rt_set_periodic_mode();
    base_period = start_rt_timer(nano2count(BASE_PERIOD_NS));
    rt_printk("base_period : %lld.\n\n",base_period);
        
    base_period_ns=count2nano(base_period);
    rt_printk("base_period_ns : %lld.\n\n",base_period_ns);
    
    // base_period_us=count2nano(base_period)/1000;
    temp=base_period_ns;
    do_div(temp,1000);
    base_period_us=temp;
    rt_printk("base_period_us : %lld.\n\n",base_period_us);

    
    // calibrate busy sleep
    rt_task_init(&init_task_str, init, 0, STACK_SIZE, 100, 0, 0);
    rt_task_resume(&init_task_str);
    
    // start dummy tasks immediately
    for (i = 0; i < NTASKS; i++) {
         task_arg[i]=i;
         rt_task_init(&tasks[i], dummy_task, task_arg[i], STACK_SIZE, task_priority[i], 1, 0);
    }    
    starttime=rt_get_time()+1000000;
    for (i = 0; i < NTASKS; i++) {
        rt_task_make_periodic(&tasks[i], starttime,task_period[i]*base_period);         
    }

    printk("End of init_module\n");    
    return 0;
}


void cleanup_module(void)
{
    int i;  

    stop_rt_timer();
    rt_sem_delete(&sync);
    printk("\n\n");
    for (i = 0; i < NTASKS; i++) {
        rt_task_delete(&tasks[i]);
    }
    rt_task_delete(&init_task_str);
}




