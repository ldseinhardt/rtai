/*
    busysleep.c
         runs in a dummy_task a busy_sleep for 200 microseconds
*/


#include <linux/kernel.h>    /* decls needed for kernel modules */
#include <linux/module.h>    /* decls needed for kernel modules */
#include <linux/version.h>    /* LINUX_VERSION_CODE, KERNEL_VERSION() */
#include <linux/errno.h>    /* EINVAL, ENOMEM */

/*
  Specific header files for RTAI, our flavor of RT Linux
 */
#include "rtai.h"
#include "rtai_sched.h"
#include <rtai_sem.h>

MODULE_LICENSE("GPL");


/* globals */

#define CALIBRATION_PERCENTAGE  100
#define CALIBRATION_LOOP_SIZE  1e5
                            // or 1000000000LL

#define STACK_SIZE 2000
#define NTASKS 1
#define HIGH 100
#define MID 101
#define LOW 102

static RTIME count_need_for_one_us_busysleep;
static SEM sync;
static RT_TASK tasks[NTASKS];
static int switchesCount[NTASKS];

static RT_TASK init_task_str;

// executes a loop from 0 till count
//  returns : time in ns to execute the loop
inline RTIME loop(RTIME count) {
    unsigned long int i;
    RTIME starttime;
    RTIME sleeptime_ns;

    //rt_printk("Do counter loop from 0 to %lld.\n",count);
    starttime = rt_get_time_ns();
    for (i = 0; i < count ; i++) {}
    sleeptime_ns=rt_get_time_ns()-starttime;

    return sleeptime_ns;
}    

/* function to callibrate busysleep function */
void calibrate_busysleep(void)
{
    RTIME sleeptime_ns;
    RTIME x;
    

    volatile RTIME loop_size=CALIBRATION_LOOP_SIZE;

    sleeptime_ns=loop(loop_size);   
  
    rt_printk("sleeptime_ns=%lld\n",sleeptime_ns);
    
    // EXPLANATION CALCULATION :
    //    sleeptime_in_us = sleeptime_ns/1000 
    //    count_need_for_one_us_busysleep=calibration_loop_size/sleeptime_in_us;
    //     -> add calibration factor to sleeptime_in_us :
    //           sleeptime_in_us -> sleeptime_in_us *  calibrationpercentage/100
    //     -> combine everything 
    //        counterbusysleepns= calibration_loop_size*10*calibrationpercentage/sleeptime_ns
    x=CALIBRATION_LOOP_SIZE*10*CALIBRATION_PERCENTAGE;
    do_div(x,sleeptime_ns);

   
    // set global calibration value
    count_need_for_one_us_busysleep =x;
}


/*
    RTIME busysleep(RTIME sleeptime_us ) 

      usage :    
          sleeptime_ns busysleep(sleeptime_us) 
    
      description :    
          keep the processor busy in a loop for a sleeptime_us amount 
          of microseconds

      returns :       
          the real time passed during this loop  (if other tasks get
          scheduled during this function this time can become much larger than
          the time this function keeps the cpu busy)
*/          
RTIME busysleep(RTIME sleeptime_us ) {
    RTIME sleeptime_ns;
    RTIME sleep_count;

    sleep_count= count_need_for_one_us_busysleep*sleeptime_us;
    sleeptime_ns=loop(sleep_count);   
    return sleeptime_ns;
}

// calibrate busy sleep
static void init(long arg)
{
    rt_printk("------------------------------------------ init task  started\n",arg);

    // first calibrate busysleep for the speed of the current machine
    // no other tasks are allowed to run to garantee optimal calibration
    calibrate_busysleep();
    rt_printk("count_need_for_one_us_busysleep=%lld\n", count_need_for_one_us_busysleep); 
     
    rt_printk("------------------------------------------ init task  ended\n",arg);
    return;
}


// task which calibrates busysleep and tests it
static void dummy_task(long arg)
{
    RTIME sleeptime_ns;
    RTIME sleeptime_us;

    rt_printk("RESUMED TASK #%d (%p) ON CPU %d.\n", arg, &tasks[arg], hard_cpu_id());
    rt_sem_wait(&sync);
    
    rt_printk("------------------------------------------ task %d started\n",arg);

    //  do a test sleep
    rt_printk("\nTEST SLEEP for 1000 us\n");
    sleeptime_us=1000;
    sleeptime_ns=busysleep(sleeptime_us);
    //  as long nothing else is happening on this machine, the returned
    //  realtime should match the  time the processor is kept busy
    rt_printk("RESULT SLEEPTIME in ns : %lld.\n\n",sleeptime_ns);
     
    rt_printk("END TASK #%d (%p) ON CPU %d.\n", arg, &tasks[arg], hard_cpu_id());
    rt_printk("------------------------------------------ task %d ended\n",arg);
    return;
}


static void signal(void)
/* signal handler, executed when a context switch occurs */
{
    RT_TASK *task;
    int i;
    if ( NTASKS > 1 )  {
        for (i = 0; i < NTASKS; i++) {
            if ((task = rt_whoami()) == &tasks[i]) {
                switchesCount[i]++;
                rt_printk("Switch to task #%d (%p) on CPU %d.\n", i, task, hard_cpu_id());
                break;
            }
        }
    }
}


// start the realtime tasks with a specific scheduling
int init_module(void)
{
    
    printk("Start of init_module\n");
    
    rt_set_oneshot_mode();
    start_rt_timer(1);
    
    printk("INSMOD on CPU %d.\n", hard_cpu_id());
    rt_sem_init(&sync, 0);

    // calibrate busy sleep
    rt_task_init(&init_task_str, init, 0, STACK_SIZE, LOW, 0, 0);
    rt_task_resume(&init_task_str);
   
    // start busysleep task
    rt_task_init(&tasks[0], dummy_task, 0, STACK_SIZE, LOW, 0, 0);
    rt_task_resume(&tasks[0]);
    rt_sem_broadcast(&sync);
    
    printk("End of init_module\n");
    
    return 0;
}


void cleanup_module(void)
{
    int i;  

    stop_rt_timer();
    rt_sem_delete(&sync);
    for (i = 0; i < NTASKS; i++) {
        printk("number of context switches task # %d -> %d\n", i, switchesCount[i]);
        rt_task_delete(&tasks[i]);
    }
    rt_task_delete(&init_task_str);
}

